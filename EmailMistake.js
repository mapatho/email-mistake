/**
 * Check if the user has not made a mistake in his email, if so display an error message
 * TODO : Si il y a un ajout il faut aussi mettre à jour sur la web-app et le site édito.
 */
function emailMistake (email) {
    const errorExtension = [
        "fe",
        "dr",
        "con",
        "cim",
        "vom",
        "vim",
        "cin",
        "cok",
        "nrt",
        "nzt",
        "ner",
        "ney",
        "nee",
        "met",
        "mrt",
        "mzt"
    ];

    const mistake = [
        "@gnail.",
        "@gmal.",
        "@gmqil.",
        "@gmai.",
        "@gmsil.",
        "@gmaim.",
        "@gmaik.",
        "@hotmal.",
        "@hotnail.",
        "@hotmaik.",
        "@hotmqil.",
        "@hmail.",
        "@hogmail.",
        "@outlok.",
        "@outlouk.",
        "@ilcoud.",
        "@ucloud.",
        "@lacoste.",
        "@lapste.",
        "@lappste.",
        "@yaooh.",
        "@yhaoo.",
        "@orqgne.",
        "@irange.",
        "@sdr.",
        "@sfrf."
    ];

    //keep the end of email after @
    let tmp = email.split("@");
    const mailDomain = tmp[tmp.length - 1];

    //keep extension email
    tmp = mailDomain.split(".");
    const extension = tmp[tmp.length - 1];
    const messagerie = `@${tmp[0]}.`;

    if (!!mistake.includes(messagerie) || !!errorExtension.includes(extension) || mailDomain === "gmail.fr")
        return true;
    return false;

}

export {emailMistake};